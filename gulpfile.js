var gulp = require("gulp"),
	sass = require("gulp-sass"),
	replace = require("gulp-replace"),
	babel = require("gulp-babel"),
	rename = require("gulp-rename"),
	uglify = require("gulp-uglify");

// converts jsx to regular javascript
gulp.task("build-jsx", function() {
    gulp.src("client/views/**/*.jsx")
        .pipe(babel())
        .on('error', function(e) { 
        	console.error(e.description + '\n' + e.fileName + '\n' + e.message); 
        })
        .pipe(gulp.dest('client/bin/views'));
});


// converts jsx to regular javascript
gulp.task("build-client", function() {
    gulp.src("client/services/**/*.js")
        .pipe(gulp.dest('client/bin/services'));
});



gulp.task("build-styles", function() {

    // builds css from sass
    gulp.src("client/style/**/*.scss")
        .pipe(sass({ errLogToConsole: true }).on("error", sass.logError))
        .pipe(gulp.dest("client/bin/style"));

    // copy over yahoos css library PureCSS
    gulp.src("./client/style/**/*.css")
        .pipe(gulp.dest("client/bin/style"))
        .on("error", function(e) {
            console.log(e);
        });
});

// task copies and minifies (if necessary) any library files needed from node_modules
gulp.task("build-lib", function() {

	// requirejs (from node_modules into dest/lib)
    gulp.src("./node_modules/requirejs/require.js")
        .pipe(uglify())
        .pipe(rename("require.min.js"))
        .pipe(gulp.dest("client/bin/lib"))
        .on("error", function(e) {
            console.log(e);
        });

    // react (from node_modules into dest/lib)
    gulp.src("./node_modules/react/dist/react.js")
        .pipe(gulp.dest("client/bin/lib"))
        .on("error", function(e) {
            console.log(e);
        });

    // react-min (from node_modules into dest/lib)
    gulp.src("./node_modules/react/dist/react.min.js")
        .pipe(gulp.dest("client/bin/lib"))
        .on("error", function(e) {
            console.log(e);
        });

    // react-dom (from node_modules into dest/lib)
    gulp.src("./node_modules/react-dom/dist/react-dom.min.js")
        .pipe(gulp.dest("client/bin/lib"))
        .on("error", function(e) {
            console.log(e);
        });
});

gulp.task("build-domain-for-web", function() {

	// we want to be able to use our domain objects on the server AND client, so we
	// (1) wrap each one in some boilerplate that converts it to an AMD style module
	// (2) change the path from "./object.js" to "model/object" which is what the browser likes
	// see: http://requirejs.org/docs/commonjs.html#manualconversion
	gulp.src(["domain/**/*.js"])
		// require("./object.js") ==> require("model/object")
		.pipe(replace(/(require\(['|"]\.\/)(.*)(\.js["|']\))/g, "require(\"model/$2\")"))
		// the next two lines wrap the commonJS module in a requireJS module conversion boilerplate
		.pipe(replace(/^/, "define(function(require, exports, module) {\n"))
		.pipe(replace(/$/, "\n});"))
		.pipe(gulp.dest("client/bin/model"));
});

gulp.task("build", ["build-jsx", "build-lib", "build-domain-for-web", "build-styles", "build-client"], function() {

	// simply copy over init.js, which is the entry point for the app
    gulp.src("init.js")
        .pipe(gulp.dest("client/bin"));
});

gulp.task('default',function() {
	// monitor the views and domain folders, build when changes detected
    gulp.watch(["client/views/**/*.jsx", "domain/**/*.js", "client/style/**/*.*css", "client/services/**/*.js"], ["build"]);
});
