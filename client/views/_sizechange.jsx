define(["react", "services/store"], function(React, Store) {

	var SizeChangeView  = React.createClass({

		render: function() { 
			var rowStyle = { "height": (100 / 3) + "%" };
			var size = this.props.size || 3,
				sizeDesc = size + " x " + size;
			return (
				<div>
					<div>
						<span>Board size (max 5)</span>
					</div>
					<input type="text" placeholder="Size" value={this.props.size} onChange={this.handleSizeChange} />
					<span className="size-board-desc">A {sizeDesc} board will be created.</span>
				</div>
			); 
		},

		handleSizeChange: function(e) {
			// allow empty string
			var value;
			if ((value = e.target.value) === "") return this.props.sizeDidChange("");
			
			// otherwise, only 1-24 are allowed
			var validated = parseInt(value);
			if (validated > 0 && validated < 6) this.props.sizeDidChange(validated);
		}
	});

	return SizeChangeView;
});
