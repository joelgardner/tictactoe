define(["react"], function(React) {

	var OpponentIndicatorView  = React.createClass({

		render: function() { 
			return (
				<div className={"opponent-indicator " + (this.props.duration ? "opponent-indicator-anim" : "hide")}>
					
				</div>
			); 
		}

	});

	return OpponentIndicatorView;
});
