define(["react", "services/store", "views/_sizechange"], function(React, Store, SizeChangeView) {

	var MainView  = React.createClass({
		
		getInitialState: function() {
			return {
				size: 3
			};
		},

		render: function() { 
			var rowStyle = { "height": (100 / 3) + "%" };
			var size = this.state.size || 3,
				sizeDesc = size + " x " + size;
			return (
				<div className="gameboard">
					<h2>Welcome to Tic Tac Toe!</h2>
					<div id="start-game" className="pure-form">

						<fieldset>
							<SizeChangeView sizeDidChange={this.handleSizeChange} size={this.state.size} />
							<button className="button-success pure-button" onClick={this.startNewGame}>Start!</button>
						</fieldset>
					</div>
				</div>
			);
		},

		startNewGame: function(e) {
			Store.startGame(this.state.size, function(id) {
				window.location.hash = "/games/" + id;
			});
		},

		handleSizeChange: function(size) {
			this.setState({ size: size });
		}
	});

	return MainView;
});
