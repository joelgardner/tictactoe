define(["react"], function(React) {

	var FeedbackView  = React.createClass({

		render: function() { 

			if (this.props.error)  {
				return <span className="error">{this.props.error}</span>;
			}

			if (this.props.status === 2) {
				return <span className={"win " + this.props.player}>{this.props.player} wins!</span>;
			}

			if (this.props.status === 1) {
				return <span>It is {this.props.player + "'s"} turn.</span>;
			}

			return <span>Tied!  Cat{"'"}s game.</span>;
		}
		
	});

	return FeedbackView;
});
