define(["react", "model/tictactoe", "services/store", "views/_sizechange", "views/_opponentindicator", "views/_feedback"], function(React, TicTacToe, Store, SizeChangeView, ComputerOpponentIndicator, FeedbackView) {

	var BoardView  = React.createClass({

		getInitialState: function() {
			return {
				size: 3,
				autoplay: true
			}
		},

		componentDidMount: function() {
			// this really would be provided via a real flux store ideally
			Store.getGame(this.props.id, (function(game) {
				if (!game) {
					return window.location.hash = "/";
				}

				this.setState({ game: game, size: game.board.state.length });
				this.makeAutoplayAfterSeconds(1.5);
			}).bind(this));
		},

		render: function() { 
			// TODO: put up a loading screen
			if (!this.state || !this.state.game || !this.state.game.board) return <h3>Loading your game...</h3>;

			// build our tic tac toe board, it is just a table
			var game = this.state.game;
			var length = (100 / game.board.state.length) + "%";
			var board = game.board.state.map(function(row, y) {
				return (
					<tr key={y}>
						{row.map(function(cell, x) {

							// is the square not taken and is the game in Ongoing status?  if so allow playing
							var mark = this.getCellMark(cell),
								handler = mark === "" && game.status() === 1 ? this.play.bind(this, x, y) : undefined;
							return (
								<td className={"cell" + mark} key={(y * 10) + x} style={{ "height": length }} onClick={handler}>
									{mark}
								</td>
							);
						}, this)} 
					</tr>
				);
				
			}, this);

			return (
				<div className="gameboard">
					<FeedbackView status={this.state.game.status()} player={this.state.game.player()} error={this.state.error} />
					<ComputerOpponentIndicator duration={this.state.secondsUntilAiPlays} />
					<table>
						<tbody>
							{board}
						</tbody>
					</table>
					<SizeChangeView sizeDidChange={this.handleSizeChange} size={this.state.size} />
					<button className="reset-btn button-success pure-button" onClick={this.resetGame}>Reset</button>
					<div style={{marginTop:'10px'}}>
						<input type="checkbox" id="autoplay" checked={this.state.autoplay} onChange={this.toggleAutoplay} /><label for="autoplay">Play against the computer</label>
					</div>
					
				</div>
			);
		},

		getCellMark: function(code) {
			var dict = {
				"1": "X",
				"-1": "O"
			};

			return dict[code] || "";
		},

		play: function(x, y) {
			var info = { id: this.props.id };
			info.x = x;
			info.y = y;
			Store.play(info, (function(payload) {
				var result = payload.result;
				if (result.error) {
					return this.setState({ error: result.error });
				}

				this.setState({ game: payload.game });
				this.makeAutoplayAfterSeconds(1.5)
			}).bind(this));
		},

		resetGame: function() {
			Store.reset(this.props.id, this.state.size, (function(info) {
				this.setState({ game: info.game, error: null });
			}).bind(this));
		},

		handleSizeChange: function(size) {
			this.setState({ size: size });
		},

		toggleAutoplay: function(e) {
			var turnAutoplayOn = e.target.checked;
			this.setState({ autoplay: turnAutoplayOn });

			// if autoplay is on and it is O's turn, make a play
			if (turnAutoplayOn) {
				this.makeAutoplayAfterSeconds();
			}
		},

		makeAutoplayAfterSeconds: function(seconds) {
			// if autoplay is on and it is O's turn, make a play
			if (!this.state.autoplay || this.state.game.player() !== "O" || this.state.game.status() !== 1) {
				return;
			}

			// setting this state will cause the indicator to start animating
			if (seconds) {
				this.setState({ secondsUntilAiPlays: seconds });
			}

			// it seems unnerving to just immediately make play
			// after the user does, so allow the caller to specify a timeout
			setTimeout((function() {

				// grab a suggestion from the game and play it
				// a suggestion has an x and y coordinate for which the play will be made
				var suggestion = this.state.game.suggest();
				suggestion.id = this.props.id;
				Store.play(suggestion, (function(payload) {
					var result = payload.result;
					if (result.error) {
						return this.setState({ error: result.error });
					}

					this.setState({ game: payload.game, secondsUntilAiPlays: undefined });
				}).bind(this));

			}).bind(this), (seconds || 0) * 1000);
		}
	});

	return BoardView;
});
