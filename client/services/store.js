define(["services/request", "model/tictactoe"], function(Request, TicTacToe) {

	var Store = {
		
		startGame: function(size, callback) {
			Request.post("/api/games?size=" + (size || 3), null, callback);
		},

		getGame: function(id, callback) {

			Request.get("/api/games/" + id, function(gameStr) {
				if (!gameStr) 
					return callback(null);

				var game = JSON.parse(gameStr),
					ttt = new TicTacToe();
				if (game.state) {
					var serialized = ttt.decode(game.state);
					ttt.setState(serialized);
				}
				callback(ttt);
			});
		},

		play: function(playInfo, callback) {
			Request.post("/api/games/" + playInfo.id + "?x=" + playInfo.x + "&y=" + playInfo.y, null, (function(gameStr) {
				var info = JSON.parse(gameStr);
				info.game = this._gameFromState(info.state);
				callback(info);
			}).bind(this));
		},

		reset: function(id, size, callback) {
			Request.post("/api/games/" + id + "/reset?size=" + (size || 3), null, (function(gameStr) {
				var game = JSON.parse(gameStr);
				var info =  {
					info: game,
					game: this._gameFromState(game.state)
				};
				callback(info);
			}).bind(this));
		},

		_gameFromState: function(state) {
			if (!state) throw Error("bad state: " + state);
			
			var ttt = new TicTacToe();
			var serialized = ttt.decode(state);
			ttt.setState(serialized);
			return ttt;
		}
	};

	return Store;
});
