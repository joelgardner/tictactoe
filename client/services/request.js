/*
 * (c) Copyright 2015 
 * flytrap.io 
 * William Joel Gardner
 */
define([
	], 
	function() {

		var RequestService = function() {
			this.get = function(url, callback, errorCallback) {
				var req = this.create('get', url, callback, errorCallback);
				req.send();
			},

			this.post = function(url, data, callback, errorCallback) {
				this._postOrPut(url, data, callback, errorCallback, false);
			},

			this.put = function(url, data, callback, errorCallback) {
				this._postOrPut(url, data, callback, errorCallback, true);
			},

			this._postOrPut = function(url, data, callback, errorCallback, isPUT) {
				var req = this.create(isPUT ? "put" : "post", url, callback, errorCallback);
				req.setRequestHeader("Content-type","application/json; charset=utf-8");
				req.send(typeof(data) === "string" ? data : JSON.stringify(data));
			}
		};

		RequestService.prototype.create = function create(method, url, callback, errorCallback) {
			var req = new XMLHttpRequest();
			req.addEventListener("error", errorCallback, false);
			req.addEventListener("abort", this.transferCanceled, false);
			req.addEventListener("load", function(data) {
				if (req.readyState !== 4) 
					return;
				callback(data.target.response);
			}, false);
			req.open(method, url);
			return req;
		};

		RequestService.prototype.transferFailed = function transferFailed(evt) {
			console.log("An error occurred while receiving response info.");
		};

		RequestService.prototype.transferCanceled = function transferCanceled(evt) {
			console.log("The transfer has been canceled by the user.");
		};

		return new RequestService;
	}
);
