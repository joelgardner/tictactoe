define(["react", "react-dom"], function(React, ReactDOM) {

	/*
	 * NOTE: In a real application I would just use react-router for routing.  
	 * But I enjoyed the challenge of creating a tiny hash-router.
	 */

	var routeHandlers = {
		"/": function() {
			require(["views/main"], function(MainView) {
				ReactDOM.render(React.createElement(MainView, null), document.getElementById("game"));
			});
		},
		"/games/:id": function(id) {
			require(["views/board"], function(BoardView) {
				ReactDOM.render(React.createElement(BoardView, { id: id }), document.getElementById("game"));
			});
		}
	}

	// handle a hash change and route to the correct view
	function hashChange() {

		// find the view we want to display and render it via React
		var route = matchHash(window.location.hash) || {};
		routeHandlers[route.key || "/"].apply(this, route.params);

		// Helper method: takes the current hash value and maps it to a route
		// so we can find the correct view to display
		function matchHash(hash) {

			// for convenience, remove the #
			if (hash[0] === "#") hash = hash.substring(1, hash.length);
			var source = hash.split("/");

			// we must check each route and find ones that match the current hash. 
			// i.e., if the hash is /games/123, we want to match /games/:id
			for(var p in routeHandlers) {

				// now we walk down each piece of the path
				// and if it starts with a ":" we know it is a parameter
				var pieces = p.split("/"),
					params = [];
				for(var i = 0;i < pieces.length;++i) {
					// dumb check against length of path
					if (pieces.length !== source.length) break;

					// if the two items don't match and one isn't a parameter, duck out
					if (pieces[i] !== source[i] && pieces[i][0] !== ":") break;

					// return the parameter value 
					if (pieces[i][0] === ":") {
						params.push(source[i]);
					}

					// if we made it to the end of the list, we have a match
					if (i === pieces.length - 1) return { key: p, params: params };
				}
			}

			// no match found
			return false;
		}
	}

	// hook everything up and trigger the first render
	window.onhashchange = hashChange;
	hashChange();
});
