var TicTacToe = require("./tictactoe.js"),
	Enum = require("./enum.js");

function Board(options) {
	options = options || {};
	this.size = options.size || 3;
	this.players = options.players;
	this.reset();
}

Board.prototype.player = function() {
	// the current player's index is equal to the number of previous plays modulo 2
	var plays = this.plays;
	if (this.status === Enum.BoardStatus.Victory) --plays;
	return this.players[plays % 2];
}

Board.prototype.play = function(x, y) {

	// if the play is no good, let the user know
	var err = this.validate(x, y)
	if (err) return { error: err, player: this.player() };

	// the key: set the player's value (either 1 or -1) at the coordinates and update the # of plays
	this.state[y][x] = this.player().value;
	++this.plays;

	// set the status and return the result of any winning coords
	var result = this.setStatus();
	return result;
}

Board.prototype.setStatus = function() {
	var result = {},
		winningCoordinates = this.isWon();

	// check for a win after the move, setting any status or winning coordinates
	if (winningCoordinates) {
		this.status = Enum.BoardStatus.Victory;
		result.coordinates = winningCoordinates;
	} 
	else if (this.isTied()) {	// or maybe the cat won
		this.status = Enum.BoardStatus.Tie;
	}

	result.status = this.status;
	return result;
}

Board.prototype.isTied = function() {
	return this.plays === (this.size * this.size) && !this.isWon();
}

Array.prototype.sum = function() {
	return this.reduce(function(prev, next) {
		return prev + next;
	}, 0);
}

Board.prototype.sumColumn = function(i) {
	var sum = 0,
		state = this.state,
		size = this.size;
	for(var j = 0;j < size;++j)
		sum += state[j][i];

	return sum;
}

///
/// Checks the board for a winning condition and if found, returns the winning coordinates.
///
Board.prototype.isWon = function() {

	var state = this.state,
		size = this.size;

	// optimization.  on a NxN board, at least 2N - 1 moves must be made for there to be a winner
	if (this.plays < (size << 1) - 1) return false;

	// when we return the winning coordinates, we actually reverse them
	// because the API goes X, Y but the state array is accessed via Y, X

	// horizontal or vertical win
	for(var i = 0;i < size;++i) {
		var horizontalSum = state[i].sum();
		if (Math.abs(horizontalSum) === size) 
			return [[0, i], [size - 1, i]];

		var verticalSum = this.sumColumn(i);
		if (Math.abs(verticalSum) === size) 
			return [[i, 0], [i, size - 1]];
	}

	// diagonal win
	for(var i = 0, topleftbottomrightSum = 0, toprightbottomleftSum = 0;i < size;++i) {
		// top left > bottom right
		topleftbottomrightSum += state[i][i];
		
		// top right > bottom left
		toprightbottomleftSum += state[i][size - i - 1];
	}

	// does top left > bottom right sum to the size of the board?
	if (Math.abs(topleftbottomrightSum) === size) 
		return [[0, 0], [size - 1, size - 1]];

	// top right > bottom left sum to the size of the board?
	if (Math.abs(toprightbottomleftSum) === size) 
		return [[size - 1, 0], [0, size - 1]];

	// found no winning line
	return false;
}

Board.prototype.validate = function(x, y) {
	if (!this.coordinatesAreOnTheBoard(x, y)) {
		return "Coordinates (" + x + ", " + y + ") are not on the " + this.size + "x" + this.size + " board.";
	}

	if (this.coordinatesAreAlreadyMarked(x, y)) {
		return "Coordinates (" + x + ", " + y + ") have already been played.";
	}

	if (this.status !== Enum.BoardStatus.Ongoing) {
		return "Invalid play, the game has been won.";
	}
}

Board.prototype.coordinatesAreOnTheBoard = function (x, y) {
	return x > -1 && x < this.size && y > -1 && y < this.size;
}

Board.prototype.coordinatesAreAlreadyMarked = function(x, y) {
	return this.state[y][x] !== 0;
}

///
/// Used to build our data-structure, which is just an array of (zero'd out) arrays, representing the board.
/// A board like this:  				Is represented like this:
/// _X_|_O_|_O_ 							[[1, -1, -1]
/// ___|_X_|___								 [0,  1,  0]
///    |   | X 								 [0,  0,  1]]
/// X's are 1s and O's are -1s.
/// This allows us to easily detect winning conditions (in a NxN board, we find a row or column (or diagonal) whose sum is N).
/// This also allows the board to be encoded simply adding 1 to the value in each cell, and concatening from top-down, left-to-right
/// The above example's encoding is 200121112
///
Board.prototype.reset = function(size) {

	// if size is not passed in, use the previously defined size
	size = size || this.size;

	// if state is undefined OR not the correct size, i.e., reset(10) was called and the size is currently 3
	// re-create the array object of the correct length
	if (!this.state || this.state.length !== size) this.state = Array(size);

	// iterate through each row, pop
	for(var i = 0;i < size;++i) {

		// if the row is nil, create an appropriately sized array
		if (!this.state[i] || this.state[i].length !== size) this.state[i] = Array(size);

		// prepopulate to 0
		for(var j = 0;j < size;++j) 
			this.state[i][j] = 0;
	}

	this.size = size;
	this.status = Enum.BoardStatus.Ongoing;
	this.plays = 0;
}

///
/// Sample output:
///
/// X | X | X
///	--|---|--
///	O |   |  
///	--|---|--
///	O |   |  
///
Board.prototype.render = function() {
	
	var colseparator = [];

	// join each value in a row with a newline
	return this.state.map(function(row, i) {
		
		// if this is not the last entry in the array, add a column marker to the separator
		if (i !== this.state.length - 1) colseparator.push("--|-");

		// join each row with a |
		return row.map(function(cell) {

			// if the space has not been played, return a space to keep the width
			if (!cell) return " ";

			// the value of the space corresponds to a player, so return that players mark (generally X or O)
			return this.players[0].value === cell 
				? this.players[0].mark 
				: this.players[1].mark;

		}, this).join(" | ");
	}, this).join("\n" + colseparator.join("") + "-\n");
}

///
/// returns a ternary (0..2) string representing the board
///
Board.prototype.serialize = function() {
	var size = this.size,
		serial = Array(size * size);

	for(var i = 0, l = size;i < l;++i) {
		for(var j = 0;j < l;++j)  {
			serial.push(this.state[i][j] + 1);
		}
	}
	
	return serial.join("");
}

///
/// Parses the serialized version of the board into a base16 integer using (much) less storage than ternary.
///
Board.prototype.encode = function() {
	// to encode a board, we simply convert its serialized version, which is a ternary number
	// to a hex #
	return parseInt(this.serialize(), 3).toString(16);
}

Board.prototype.decode = function(base16str) {
	// take the base-16 str given and convert it to its ternary equivalent
	if (typeof(base16str) !== "string") throw Error("bad input for decoding: " + base16str);

	// parse the hex string into an integer value and then convert it to a ternary string
	var str = parseInt(base16str, 16).toString(3);

	// we want to pad with 0's if the length of the str is not a perfect square
	// this can happen if, say, the top row is all O's, which serialize to 0's, which
	// and leading zeros are lost when translated to base-3 
	// assumes a max board size of 10
	var exploded = str.split("");
	while ([1, 4, 9, 16, 25, 36, 49, 64, 81, 100].indexOf(exploded.length) < 0) {
		exploded.unshift("0");
	}
	
	return exploded.join("");
}

Board.prototype.setState = function(ternaryStr) {

	if (!ternaryStr) throw Error("bad input for setState " + ternaryStr);

	// be helpful to the caller.  if they call with a base10 number, just convert it for them
	if (typeof(ternaryStr) === "number") ternaryStr = this.decode(ternaryStr); 

	
	// we deduce the size of the board from the length of the board's
	// serialized string.  i am not convinced this is 100% fool-proof but i haven't
	// been able to break it yet
	// TODO to guarantee board size integrity: store the size in the encoding: on an NxN board, encode as N|abcd1234 (where abcd1234 is hex)
	for(var i = 1;i < 10;++i) {
		if (i * i === ternaryStr.length) {
			break;
		}
	}

	// readjust size from 
	this.reset(i);

	// set the board according to the serialized string
	for(var i = 0, l = ternaryStr.length;i < l;++i) {
		var x = i % this.size,
			y = parseInt(i / this.size);
		this.state[y][x] = ternaryStr[i] - 1;

		// increase # of plays if cell has been marked (1 or -1)
		if (ternaryStr[i] - 1) ++this.plays;
	}

	// update the status of the board
	this.setStatus();
}

module.exports = Board;
