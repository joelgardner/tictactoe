var Enums = {
	///
	/// Enum for the status of the game
	///
	BoardStatus: {
		Ongoing: 	1,
		Victory: 	2,
		Tie: 		3
	}
}

module.exports = Enums;
