///
/// Right now, the Player object is pretty bare and mainly serves as a bag of properties for now.
///

function Player(mark, value) {
	this.mark = mark;
	this.value = value;
}

module.exports = Player;
