var Board = require("./board.js"),
	Player = require("./player.js"),
	Enum = require("./enum.js");

function TicTacToe(options) {
	// use default values if not defined
	options = options || {};
	options.players = [ 
		new Player(options.X || "X",  1), 
		new Player(options.O || "O", -1) 
	];

	// create a board (if size is not defined in options, Board defaults size to 3)
	this.board = new Board(options);
}

///
/// The main method that a user will be calling to interact with the board.
/// Pass X, Y values to mark the board with the currently active player's designation (i.e., X or O)
///
TicTacToe.prototype.play = function(x, y) {
	var result = this.board.play(x, y);
	result.player = this.player();
	return result;
};

TicTacToe.prototype.reset = function(size) {
	this.board.reset(size > 0 ? size : 3);
}

TicTacToe.prototype.size = function() {
	return this.board.size;
}

TicTacToe.prototype.status = function() {
	return this.board.status;
}

TicTacToe.prototype.player = function() {
	return this.board.player().mark;
}

TicTacToe.prototype.serialize = function() {
	return this.board.serialize()
}

TicTacToe.prototype.encode = function() {
	return this.board.encode();
}

TicTacToe.prototype.decode = function(base10int, setState) {
	return this.board.decode(base10int, setState);
}

TicTacToe.prototype.setState = function(ternaryStr) {
	return this.board.setState(ternaryStr);
}

///
/// Draws the board in its current state.  
///
TicTacToe.prototype.draw = function() {

	// build a nice description of the current state of the game
	var status = this.status(),
		player = this.board.player(),
		description;

	switch (status) {
		case Enum.BoardStatus.Ongoing: description = player.mark + "'s turn."; break;
		case Enum.BoardStatus.Victory: description = player.mark + " wins! " + JSON.stringify(this.board.isWon()); break;
		case Enum.BoardStatus.Tie: description = "Cat wins."; break;
	}

	// log to console a (poorly drawn) board with X's and O's
	var separator = "=================================================\n";
	console.log(separator + description + "\n" + this.board.render() + "\n");
}

Array.prototype.sum = function() {
	return this.reduce(function(prev, next) {
		return prev + next;
	}, 0);
}

///
/// SUGGESTIONS / "AI"
///
/// walk each cell, and find the one whose sum (going horizontal OR vertical) is the highest
/// for example, the asterisk marks below the cell that would be chosen, because
/// its vertical value would be Math.abs(-2) (two O's)
/// _X_|_O_|___
/// ___|_O_|_X_
///    | * |
TicTacToe.prototype.suggest = function() {

	var maxSum = 0,
		results = [0, 0],
		size = this.size(),
		state = this.board.state,
		tlbr_cell = null, bltr_cell = null,
		tlbr_sum = 0, bltr_sum = 0;

	// first walk each cell, and find the one whose sum (going horizontal OR vertical) is the highest
	for(var i = 0;i < size;++i) {
		for(var j = 0;j < size;++j) {

			// keep track of the sum of the top-left to bottom-right diagonal and a free cell
			if (i === j) {
				tlbr_sum += state[i][i];
				if (!state[i][i]) tlbr_cell = [i, i];
			}

			// keep track of the sum of the bottom-left to top-right diagonal and a free cell
			if (i + j === size - 1) {
				bltr_sum += state[j][i];
				if (!state[j][i]) bltr_cell = [j, i];
			}

			// if square is taken, skip
			if (state[i][j]) continue;

			// check row and col sums for the max row
			var c = [j, i];
			testCellForIdealMove(state[i].sum(), c);			// row
			testCellForIdealMove(this.board.sumColumn(j), c);	// col
		}
	}

	// diagonals
	testCellForIdealMove(tlbr_sum, tlbr_cell);
	testCellForIdealMove(bltr_sum, bltr_cell);

	// optimization: if the MAX sum is 1 and the middle is open, take the middle.  bwahahaha
	if (maxSum === 1 && size % 2) {
		var middle = parseInt(size / 2);
		if (!state[middle][middle]) {
			results = [middle, middle];
		}
	}

	return { x: results[0], y: results[1] };

	// helper function for testing the cell for whether it should be suggested
	function testCellForIdealMove(sum, cell) {
		var abs = Math.abs(sum);
		if (!cell || abs < maxSum)
			return;

		// if we equaled our max AND it would be a winning play, use this cell
		if (abs === maxSum && abs === size - 1) {
			var player = this.player();
			if ((player === "O" && sum > 0) || (player === "X" && sum < 0))
				return; // if we're here, it is a defensive -- not a winning -- play
		}

		// set the new max and the current cell as the result
		maxSum = abs;
		results = cell;
	}
}

module.exports = TicTacToe;
