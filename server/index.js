"use strict";

var koa = require("koa"),
	route = require("koa-route"),
	app = koa(),
	service = require("./data/tictactoeservice.js"),
	static_files = require('koa-static');

// serve static files on port 3001
// these are served via nginx over port 8083
app.use(static_files("./client"), { defer: true });

// GET /api/games/:id
app.use(route.get("/api/games/:id", function *getGame(id) {
	var game = yield service.games.find(id);
	this.body = game;
}));

// POST /api/games?size=3
app.use(route.post("/api/games", function *newGame() {
	var newgame = yield service.games.create({ size: this.query.size || 3 });
	this.body = newgame.id;
}));

// POST /api/games/abc123?x=0&y=1
app.use(route.post("/api/games/:id", function *playGame(id) {
	var game = yield service.games.play(id, this.query.x, this.query.y);
	this.body = game;
}));

// POST /api/games/abcd123/reset?size=4
app.use(route.post("/api/games/:id/reset", function *resetGame(id) {
	var game = yield service.games.reset(id, this.query.size || 3);
	this.body = game;
}));

// kick off server
app.listen(3001);
