///
/// For simplicity, we're just using an in-memory cache to store all game data.
/// This allows us to cut down on setup.
///
var _nodecache = require("node-cache"),
	_cache = new _nodecache({ 
		stdTTL: 24 * 60 * 60, 		// each game lives 24 hours
		checkperiod: 1 * 60 * 60,  	// sweep for expired games every hour
		useClones: false 			// by reference, not value
	});

module.exports = {
	
	get: function(key) {
		return new Promise(function(resolve, reject) {
			_cache.get(key, function(err, value) {
				if (err) return reject(err);
				return resolve(value);
			});
		});
		
	},

	set: function(key, value, callback) {
		return new Promise(function(resolve, reject) {
			_cache.set(key, value, function(err, success) {
				if (err) return reject(false, err);
				return resolve(true);
			})
		});
	}
};
