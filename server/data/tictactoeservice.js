
var db = require("./db.js"),
	TicTacToe = require("../../domain/tictactoe.js");

module.exports = {
	games: {

		find: function(id) {
			return db.get(id).then(function(value) {
				return value;
			});
		},

		create: function(options) {

			// create info to persist
			var game = new TicTacToe({ size: options.size }),
				info = { id: this._createId(), state: game.encode() };

			// set the value in our in-memory cache
			return db.set(info.id, info).then(function(success) {
				if (!success) throw Error("Unable to set key " + info.id);
				return info;
			});
		},

		reset: function(id, size) {
			return this.find(id).then(function(game) {

				// now create a new game and set it to its previous state (from cache)
				var ttt = new TicTacToe({ size: size });

				// now make the play, cache the new state, and return the result
				var info = { id: id, state: ttt.encode() };
				return db.set(id, info).then(function(success) {
					return info;
				});
				
			});
		},

		play: function(id, x, y) {
			return this.find(id).then(function(game) {

				// now create a new game and set it to its previous state (from cache)
				var ttt = new TicTacToe();
				ttt.setState(ttt.decode(game.state));

				// now make the play, cache the new state, and return the result
				var result = ttt.play(x, y),
					info = { id: id, state: ttt.encode() };
				return db.set(id, info).then(function(success) {
					info.result = result;
					return info;
				});
				
			});
		},

		_createId: function() {

			// taken from: http://stackoverflow.com/questions/1349404/generate-a-string-of-5-random-characters-in-javascript
			// and tweaked a bit for performance (namely using an array + Array.join rather than string concatenation)
			var result = Array(6),
				possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			// just pick randomly
			for(var i = 0;i < result.length; i++)
				result[i] = possible.charAt(Math.floor(Math.random() * possible.length));

			// TODO: check that this Id isn't already in the cache (unlikely but possible)

			return result.join("");
		}
	}
}
