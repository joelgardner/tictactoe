"use strict";

function getThingAsync(time, callback) {
	setTimeout(function() {
		callback(time);
	}, time);
}

getThingAsync(500, function(message) {
	getThingAsync(750, function(message) {

	})
});

function run(generator) {
	var iterator = generator(resume);

	function resume(value) {
		iterator.next(value);
	}

	// set off initial wait
	iterator.next();
}

run(function *getThingsAsync(resume) {
	var a = yield getThingAsync(500, resume);
	console.log(a);

	var b = yield getThingAsync(a + 250, resume);
	console.log(b);
});
