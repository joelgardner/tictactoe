"use strict";
var assert = require("assert");
var TicTacToe = require("../domain/tictactoe.js"),
	Enum = require("../domain/enum.js");

describe('TicTacToe', function() {
	// each test will use this instance
	var game = new TicTacToe();

	// reset the board after each test
	afterEach(function() {
		game.reset(3);
	});

	describe('changing players after a play is made', function () {
		it('should change the current player when a play is made', function () {
			var playerX = game.player();
			var playerO = game.play(0, 0).player;
			assert.notStrictEqual(playerX, playerO);
		});

		it('should not change the current player when an invalid play is made', function () {
			var playerO = game.play(0, 0).player;
			var playerX = game.play(0, 0).player; 	// invalid: (0, 0) was already played, so it remains the same player's turn.
			assert.strictEqual(playerX, playerO);
		});

		it('player should not change after a winning play is made', function () {
			var playerX = game.player();
			game.play(0, 0); // X
			game.play(0, 1); // O
			game.play(1, 0); // X
			game.play(0, 2); // O
			var win = game.play(2, 0); // winning play (X's across the top)
			assert.strictEqual(win.player, playerX);
		});
	});

	describe('changing status after a play is made', function () {
		it('status should be Ongoing when a game has just started', function () {
			assert.strictEqual(game.status(), Enum.BoardStatus.Ongoing);
		});

		it('status should be Victory when a winning play is made', function () {
			game.play(0, 0); // X
			game.play(0, 1); // O
			game.play(1, 0); // X
			game.play(0, 2); // O
			var win = game.play(2, 0); // winning play (X's across the top)
			assert.strictEqual(win.status, Enum.BoardStatus.Victory);
		});

		it('status should be Victory when a topleft-bottomright diagonal winning play is made', function () {
			game.play(0, 0); // X
			game.play(0, 1); // O
			game.play(1, 1); // X
			game.play(1, 0); // O
			var win = game.play(2, 2); // winning play 
			game.draw();
			assert.strictEqual(win.status, Enum.BoardStatus.Victory);
		});

		it('status should be Victory when a topright-bottomleft diagonal winning play is made', function () {
			game.reset(4);
			game.play(3, 0);
			game.play(1, 3);
			game.play(2, 1); // X
			game.play(0, 1); // O
			game.play(1, 2); // X
			game.play(1, 0); // O
			var win = game.play(0, 3); // winning play (X's across the top)
			assert.strictEqual(win.status, Enum.BoardStatus.Victory);
		});

		it('status should be Tie when the board is filled and there\'s no winner', function () {
			game.play(0, 0); // X
			game.play(1, 0); // O
			game.play(0, 1); // X
			game.play(0, 2); // O
			game.play(2, 1); // X
			game.play(2, 2); // O
			game.play(1, 2); // X
			game.play(1, 1); // O
			var tie = game.play(2, 0); // X
			assert.strictEqual(tie.status, Enum.BoardStatus.Tie);
		});
	});

	describe('board size', function () {
		it('resetting sets the size of board', function () {
			game.reset(4);
			assert.strictEqual(game.board.size, 4);
		});

		it('resetting to size 0 sets the size of board to 3', function () {
			game.reset(0);
			assert.strictEqual(game.board.size, 3);
		});

		it('resetting to size -1 doesn\'t break stuff and sets the board size to 3', function () {
			game.reset(-1);
			assert.strictEqual(game.board.size, 3);
		});

		it('first play in a 1x1 game wins', function () {
			game.reset(1);
			var win = game.play(0, 0);
			assert.strictEqual(win.status, Enum.BoardStatus.Victory);
		});

		it('winning a 4x4 board', function () {
			game.reset(4);
			game.play(0, 0);
			game.play(1, 0); // O
			game.play(0, 1);
			game.play(1, 1); // O
			game.play(0, 2);
			game.play(1, 2); // O
			game.play(0, 3);
			assert.strictEqual(game.board.state.length, 4);
		});		
	});

	describe('encoding and decoding', function () {
		it('should serialize to 111111111 a when no moves exist on a 3x3 board', function () {
			var encoded = game.serialize();
			assert.strictEqual(encoded, "111111111");
		});

		it('should encode to 2671 a when no moves exist on a 3x3 board', function () {
			var encoded = game.encode();
			var expected = parseInt("111111111", 3).toString(16);
			if (expected !== "2671") throw Error("bad expected value " + expected + ", should be 2671.");
			assert.strictEqual(encoded, expected);
		});

		it('should serialize to 111121111 a when the middle square is played', function () {
			game.play(1, 1);
			var encoded = game.serialize();
			assert.strictEqual(encoded, "111121111");
		});

		it('should encode to 26c2 a when no moves exist on a 3x3 board', function () {
			game.play(1, 1);
			var encoded = game.encode();

			var expected = parseInt("111121111", 3).toString(16);
			if (expected !== "26c2") throw Error("bad expected value " + expected + ", should be 26c2.");
			assert.strictEqual(encoded, expected);
		});

		it('should decode 26c2 to 111121111', function () {
			var ternary = game.decode("26c2");
			assert.strictEqual(ternary, "111121111");
		});

		it('should decode 26c2 to 111121111 and set the board correctly', function () {
			var serialized = game.decode("26c2");
			game.setState(serialized);
			assert.strictEqual(game.serialize(), "111121111");
		});

		it('after setting state to 26c2, it should be O\'s turn', function () {
			var serialized = game.decode("26c2");
			game.setState(serialized);
			assert.strictEqual(game.player(), "O");
		});

		it('after setting state to 3d92, it status should be Ongoing', function () {
			var serialized = game.decode("3d92");
			game.setState(serialized);
			assert.strictEqual(game.status(), 1);
		});

		it('after setting state to fa9, it status should be Ongoing', function () {
			var serialized = game.decode("fa9");
			game.setState(serialized);
			var result = game.play(0, 2);
			console.log("der" + game.encode());
			assert.notStrictEqual(game.encode(), Infinity);
		});

		it('decoding f61, should return a str of length 9', function () {
			var serialized = game.decode("f61");
			game.setState(serialized);
			game.draw();
			assert.strictEqual(serialized.length, 9);
		});

		it('after setting state to 2020202020202111, it should be X\'s victory', function () {
			game.setState("2020202020202111");
			assert.strictEqual(game.player(), "X");
		});

		it('211201201 encode16 should return 40ba', function () {
			game.setState("211201201");
			assert.strictEqual(game.board.encode(), "40ba");
		});


		it("should not crash when state is 1f90bb7 and suggest() is called -- specifically, when a diagonal has the highest differential but has no free cells", function(done) {
			var serialized = game.decode("1f90bb7");
			console.log(serialized);
			game.setState(serialized);
			var suggestion = game.suggest();
			game.draw();
			done();
		});

		it("should have winning coordinates of [4, 0], [4, 4] in state 1111011020102200222020220, not [0, 4], [4, 4]", function() {
			game.setState("1111011020102200222020220");
			var result = game.board.isWon()
			game.draw();

			assert.strictEqual(result[0][0], 4);
			assert.strictEqual(result[0][1], 0);
		});
	});

	describe('random', function () {
		it('error returned when playing a won game', function () {
			game.setState("211201201");
			var err = game.play(2, 2);

			var undefined;
			assert.notStrictEqual(err.error, undefined);
		});

		it('2020202020202111 maps to 1eca177', function () {
			debugger;
			game.setState("2020202020202111");
			assert.strictEqual(game.encode(), "1eca177");
		});


	});
});
